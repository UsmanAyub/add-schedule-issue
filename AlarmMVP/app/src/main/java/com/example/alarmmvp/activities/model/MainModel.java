package com.example.alarmmvp.activities.model;

import com.example.alarmmvp.activities.presenter.MainPresenter;
import com.example.alarmmvp.activities.view.MainView;

public class MainModel implements MainPresenter{

    MainView mainView;

    public MainModel(MainView mView) {
        mainView = mView;
    }

    @Override
    public void populateList(String title, String hours, String minutes, String days) {
        if(hours!=null && minutes!=null)
        {
            mainView.listPopulated();
        }
    }


}
