package com.example.alarmapp04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

public class AlarmAdapter extends ArrayAdapter<AlarmItem> {

    Context context;
    int resource;
    List<AlarmItem> list;

    public AlarmAdapter(Context context, int resource, List<AlarmItem> list) {
        super(context, resource, list);

        this.context = context;
        this.resource = resource;
        this.list = list;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);

        TextView title = view.findViewById(R.id.title);
        TextView hour = view.findViewById(R.id.hour);
        TextView minute = view.findViewById(R.id.minute);
        TextView days = view.findViewById(R.id.repeat_days);

        AlarmItem item = list.get(position);

        title.setText(item.getTitle());
        hour.setText(item.getHour()+":");
        minute.setText(item.getMinute());
        days.setText(item.getDays());

        return view;
    }
}
