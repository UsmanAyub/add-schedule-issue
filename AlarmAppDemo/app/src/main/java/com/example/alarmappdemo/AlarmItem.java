package com.example.alarmappdemo;

public class AlarmItem {
    private String title, hour, minute, days;


    public AlarmItem(String title, String hour, String minute, String days) {
        this.title = title;
        this.hour = hour;
        this.minute = minute;
        this.days = days;
    }

    public String getTitle() {
        return title;
    }

    public String getHour() {
        return hour;
    }

    public String getMinute() {
        return minute;
    }

    public String getDays() {
        return days;
    }
}
